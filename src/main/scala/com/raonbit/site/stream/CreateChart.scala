package com.raonbit.site.stream

import java.util
import java.util.Date

import org.apache.http.client.entity.UrlEncodedFormEntity
import org.apache.http.client.methods.HttpPost
import org.apache.http.entity.StringEntity
import org.apache.http.impl.client.HttpClients
import org.apache.http.message.BasicNameValuePair
import org.apache.http.util.EntityUtils
import org.omg.CORBA.NameValuePair
import scala.collection.JavaConversions._

/**
 * Created by root on 15. 11. 9.
 */
object CreateChart {
  def main(args: Array[String]): Unit = {
    create("a9cotwhk0o")
  }

  def create(streamToken: String): String = {
    val timestamp = new Date().getTime()
    val httpclient = HttpClients.createDefault()
    val httpPost = new HttpPost("https://plot.ly/clientresp")

    val nvps = new util.ArrayList[BasicNameValuePair]()
    nvps.add(new BasicNameValuePair("un", "kyhleem"))
    nvps.add(new BasicNameValuePair("key", "esnjrup9go"))
    nvps.add(new BasicNameValuePair("origin", "plot"))
    nvps.add(new BasicNameValuePair("platform", "scala"))
    //nvps.add(new BasicNameValuePair("args", "[[0],[0]]"))
    nvps.add(new BasicNameValuePair("kwargs",
      s"""{"filename":"TrafficStream_${timestamp}",
                                                   "fileopt": "extend",
                                                   "style": {"type": "scatter"},
                                                   "layout": {
                                                        "title": "StreamTest data2"
                                                    }
                                                  }"""))
    nvps.add(new BasicNameValuePair("args",
      s"""[{
                                                  "x": [],
                                                  "y": [],
                                                  "type": "scatter",
                                                  "mode": "markers",
                                                  "stream": {
                                                      "token": "${streamToken}",
                                                      "maxpoints": 500
                                                      }
                                                  }
                                                ]"""))

    httpPost.setEntity(new UrlEncodedFormEntity(nvps))

    val response = httpclient.execute(httpPost)

    try {
      println(response.getStatusLine())
      val entity = response.getEntity()
      //EntityUtils.toString(entity)
      val chartInfo = new String(EntityUtils.toByteArray(entity))
      println("ChartInfo ==>" + chartInfo)
      chartInfo
    } finally {
      response.close()
    }
  }
}

