package com.raonbit.site.stream

import java.net.Socket
import java.util.Date

import akka.actor.ActorSystem
import scala.concurrent._
import scala.concurrent
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global

/**
 * Created by root on 15. 11. 11.
 */
object SendTraffic extends App {

  val system = ActorSystem("MySystem")
  system.scheduler.schedule(0 seconds, 1 seconds) {
    val date = new Date().toString
    val data = s"""{"type":"incident","coordx":127.99809722899307,"coordy":37.38109272290238,"time": "${date}","msg":"사고 테스트 데이터", "direction":"양방향"}"""
    val socket = new Socket("localhost", 9988)
    val out = socket.getOutputStream()
    println("send ==> " + data)
    out.write(data.getBytes)
    out.close()
    socket.close()
  }
}
