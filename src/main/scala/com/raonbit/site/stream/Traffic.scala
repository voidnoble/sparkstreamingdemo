package com.raonbit.site.stream

import akka.actor.ActorSystem
import org.apache.http.NoHttpResponseException
import org.apache.http.client.methods.{HttpPost, HttpGet}
import org.apache.http.entity.StringEntity
import org.apache.http.impl.client.HttpClients
import org.apache.http.util.EntityUtils
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.apache.spark.{SparkConf, Logging}
import org.apache.spark.storage.StorageLevel
import org.apache.spark.streaming.receiver.Receiver

import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.parsing.json.{JSON, JSONObject}

/**
 * Created by root on 15. 11. 10.
 */
object Traffic extends Serializable {
  val defaultParam = Map(
    ("MinX" -> "127.800000"),
    ("MaxX" -> "127.890000"),
    ("MinY" -> "34.900000"),
    ("MaxY" -> "35.100000")
  )
  val defaultSql = "select max(generatedate) generatedate, avg(avgspeed) avgspeed from traffic"
  val streamToken = "8f0k1z2dff"

  def main(args: Array[String]): Unit = {
    val sql = "select max(generatedate) generatedate, avg(avgspeed) avgspeed from traffic"
    val params = Map(
      ("MinX" -> "127.800000"),
      ("MaxX" -> "127.890000"),
      ("MinY" -> "34.900000"),
      ("MaxY" -> "35.100000")
    )

    val chartInfo = CreateChart.create(streamToken)


    //println("Realtime Chart URL ==> " + plotly+".embed" )

    start(sql = sql, params = params)
  }

  def start(sql: String = defaultSql, params: Map[String, String] = defaultParam) {
    val conf = new SparkConf().setAppName("Traffic Streaming")
    conf.setMaster("local[*]")

    val ssc = new StreamingContext(conf, Seconds(5))
    val lines = ssc.receiverStream(new TrafficReceiver(params, 5))

    lines.foreachRDD(rdd => {

      if(rdd.count() > 0) {
        val sqlContext = new org.apache.spark.sql.SQLContext(rdd.sparkContext)
        val traffic = sqlContext.read.json(rdd)
        traffic.registerTempTable("traffic")
        //traffic.printSchema()
        val result = sqlContext.sql(sql)
        result.show()
        result.foreach(row => {
          sendData((row.get(0).toString, row.get(1).toString))
        })
      }
    })
    lines.print()
    ssc.start()
    ssc.awaitTermination()
  }


  var index = 1
  def sendData(data: Tuple2[String, String]) {
    println("post json..")

    val httpclient = HttpClients.createDefault()
    val httpPost = new HttpPost("http://stream.plot.ly.")

    //httpPost.addHeader("Transfer-Encoding", "chunked")
    httpPost.addHeader("plotly-streamtoken", streamToken)

    index += 1
    val jsonEntity = new StringEntity("{ \"x\": "+ (index) +", \"y\": "+data._2+" }\n\n")
    jsonEntity.setContentType("application/json")
    httpPost.setEntity(jsonEntity)

    val system = ActorSystem("MySystem")
    system.scheduler.scheduleOnce(1 seconds)(httpclient.close())

    try {
      val response = httpclient.execute(httpPost)
      val entity = response.getEntity()
      println(new String(EntityUtils.toByteArray(entity)))
    } catch {
      case ne: NoHttpResponseException =>
      case e: Exception => e.printStackTrace()
    }finally {
      //httpclient.close()
    }
  }
}

class TrafficReceiver(paramMap:Map[String, String], interval: Long) extends Receiver[String](StorageLevel.MEMORY_AND_DISK) with Logging {
  override def onStart(): Unit = {
    new Thread("Traffic Receiver") {
      override def run() { receive() }
    }.start()
  }
  override def onStop(): Unit = {
    println("Stop TrafficReceiver..")
  }

  def receive(): Unit = {
    val system = ActorSystem("MySystem")
    system.scheduler.schedule(0 seconds, interval seconds) {
      val jsonStr = getTrafficData()
      val list = parseJson(jsonStr)
      println(list.length)
      list.foreach { s =>
        if(!isStopped) store(s)
      }
    }
  }

  def parseJson(jsonString: String) = {
    val data = jsonString.replace("\n", "").replace("[", "").replace("]", "").replace("},", "}\n")
    data.split("\n")
  }

  def getTrafficData(): String = {
    val baseUrl = "http://openapi.its.go.kr/api/NTrafficInfo?key=1447062467480&ReqType=2&getType=json"

    val httpclient = HttpClients.createDefault()
    val httpGet = new HttpGet(baseUrl + "&" + mapToReqString(paramMap))
    val response = httpclient.execute(httpGet)

    try {
      val entity = response.getEntity()
      new String(EntityUtils.toByteArray(entity))
    } finally {
      response.close()
      httpclient.close()
    }
  }

  def mapToReqString(paramMap: Map[String, String]): String = {
    paramMap.map(p => p._1 + "=" + p._2).mkString("&")
  }

}