package com.raonbit.site.stream

import java.net.URLEncoder
import java.util
import java.util.concurrent.TimeUnit

import akka.actor.ActorSystem
import org.apache.http.Header
import org.apache.http.client.HttpClient
import org.apache.http.client.entity.UrlEncodedFormEntity
import org.apache.http.client.methods.{HttpGet, HttpPost}
import org.apache.http.entity.StringEntity
import org.apache.http.impl.client.{HttpClients, CloseableHttpClient}
import org.apache.http.message.BasicNameValuePair
import org.apache.http.util.EntityUtils
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global

/**
 * Created by root on 15. 11. 9.
 */
object SendData extends App {

  val system = ActorSystem("MySystem")
  system.scheduler.schedule(0 seconds, 1 seconds) {
    try {
      sendData()
    }  catch {
      case e: Exception => {
        println("e")
        //throw e
      }
    }
  }

  def sendData() {
    println("post json..")

    val httpclient = HttpClients.createDefault()
    val httpPost = new HttpPost("http://stream.plot.ly.")

    //httpPost.addHeader("Transfer-Encoding", "chunked")
    httpPost.addHeader("plotly-streamtoken", "a9cotwhk0o")
    val data = getData()

    val jsonEntity = new StringEntity("{ \"x\": "+data._1+", \"y\": "+data._2+" }\n\n")
    jsonEntity.setContentType("application/json")

    httpPost.setEntity(jsonEntity)

    system.scheduler.scheduleOnce(1 seconds)(httpclient.close())

    val response = httpclient.execute(httpPost)

    try {
      println(response.getStatusLine())
      val entity = response.getEntity()
      //EntityUtils.toString(entity)
      println(new String(EntityUtils.toByteArray(entity)))
    } finally {
      response.close()
    }
  }

  var x=10
  var y=0
  def getData() = {
    x = x + 1
    y = x * 2 + 10
    (x, y)
  }
}
