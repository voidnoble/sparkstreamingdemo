name := "TrafficStream"

version := "1.0"

scalaVersion := "2.10.4"

libraryDependencies ++= Seq(
  "org.apache.httpcomponents" % "httpclient" % "4.5.1",
  "org.apache.spark" %% "spark-core"    % "1.5.1"  % "provided",
  "org.apache.spark" %% "spark-sql"    % "1.5.1"  % "provided",
  "org.apache.spark" %% "spark-mllib"   % "1.5.1"  % "provided",
  "org.apache.spark" %% "spark-streaming"   % "1.5.1"  % "provided",
  "org.codehaus.jettison" % "jettison" % "1.3.7",
  "io.netty" % "netty" % "3.10.5.Final"
)