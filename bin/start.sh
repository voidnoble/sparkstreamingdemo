#!/bin/sh

HOME="$(cd `dirname $0`/..; pwd)"
cd $HOME

SPARK_HOME=/opt/raon/infra/spark
MAIN_JAR=${HOME}/target/scala-2.10/TrafficStream-assembly-1.0.jar
JARS=""

echo ${SPARK_HOME}/bin/spark-submit --master=local[*] --class com.raonbit.site.stream.TrafficAlram ${MAIN_JAR} "$@"
${SPARK_HOME}/bin/spark-submit --master=local[*] --class com.raonbit.site.stream.TrafficAlram ${MAIN_JAR} "$@"